﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HotelApi.Models;

namespace HotelApi.Controllers
{
    public class SheratonController : ApiController
    {
        SheratonEntities db = new SheratonEntities();

        //lấy danh sách phòng hiện có của khách sạn
        [HttpGet]
        public List<LoaiPhong> LayDanhSachLoaiPhong()
        {
            return db.LoaiPhong.ToList();
        }

        //trả về một ngày có nằm trong một khoảng thời gian cho trước hay không        
        public bool IsInDateRange(string dateToCheck, string date1, string date2)
        {
            DateTime _dateToCheck = DateTime.Parse(dateToCheck);
            DateTime _date1 = DateTime.Parse(date1);
            DateTime _date2 = DateTime.Parse(date2);
            return (_date1 <= _dateToCheck && _dateToCheck <= _date2);
        }

        //lấy giá tiền của phòng
        public double GiaPhong(string MaLoaiPhong)
        {
            double GiaTien = -1;
            LoaiPhong loaiPhong = db.LoaiPhong.Find(MaLoaiPhong);
            if (loaiPhong != null)
            {
                GiaTien = loaiPhong.GiaTien;
            }
            return GiaTien;
        }

        //trả về số phòng trống của loại phòng trong một khoảng thời gian nhất định
        [HttpGet]
        public int SoPhongTrong(string MaLoaiPhong,string NgayNhanPhong, string NgayTraPhong)
        {
            var dsPhongHienTai = (from p in db.Phong where p.LoaiPhong1.MaLoaiPhong == MaLoaiPhong select p).ToList();
            int SoPhongTrong = 0;
            foreach (Phong p in dsPhongHienTai)
            {
                if (PhongDangTrong(p.MaPhong.ToString(), NgayNhanPhong, NgayTraPhong))
                {
                    SoPhongTrong++;
                }
            }
            return SoPhongTrong;
        }


        //Kiểm tra xem một phòng có trống trong khoảng thời gian cho trước hay ko ?
        [HttpGet]
        public bool PhongDangTrong(String MaPhong, String NgayDau, String NgayCuoi)
        {
            Phong phongKiemTra = db.Phong.Find(int.Parse(MaPhong));
            if (phongKiemTra == null)
                return false;
            else
            {
                foreach (DatPhong dp in db.DatPhong.ToList())
                {
                    if (dp.MaPhong == phongKiemTra.MaPhong) //nếu phòng có trong danh sách đặt thì kiểm tra ngày đặt
                    {
                        if (IsInDateRange(NgayDau, dp.NgayNhanPhong, dp.NgayTraPhong) || (IsInDateRange(NgayCuoi, dp.NgayNhanPhong, dp.NgayTraPhong)))
                        {
                            return false;
                        }
                        break;
                    }
                }
                return true;
            }
        }

        public class ModelDatPhong
        {
            public String SoLuong { get; set; }
            public String NgayNhanPhong { get; set; }
            public String NgayTraPhong { get; set; }
            public String MaLoaiPhong { get; set; }
        }


        [HttpPost]
        public void DatPhong(ModelDatPhong datPhong)
        {
            var phongs = (from p in db.Phong where p.LoaiPhong == datPhong.MaLoaiPhong select p).ToList();
            var soPhongDaDat = 0;
            foreach (Phong phong in phongs)
            {
                if (PhongDangTrong(phong.MaPhong.ToString(), datPhong.NgayNhanPhong, datPhong.NgayTraPhong))
                {
                    DatPhong dp = new DatPhong();
                    dp.LoaiPhong = datPhong.MaLoaiPhong;
                    dp.NgayNhanPhong = datPhong.NgayNhanPhong;
                    dp.NgayTraPhong = datPhong.NgayTraPhong;
                    dp.MaPhong = phong.MaPhong;

                    db.DatPhong.Add(dp);
                    db.SaveChanges();

                    soPhongDaDat++;
                }
                if (soPhongDaDat == int.Parse(datPhong.SoLuong))
                {
                    break;
                }
            }
            return;
        }
    }
}
